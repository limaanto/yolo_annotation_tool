#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <system_error>

#include "argparse.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

using cv::Mat;
using cv::Point;
using cv::Rect;
using cv::Scalar;
namespace fs = std::filesystem;

struct Annotation {
  Rect rect;
  int label;
};

std::vector<std::string> gLabels;
std::vector<Annotation> gRects;

Point gPosBeg;

Mat frame;

size_t gIndex;
bool gDrawing = false;
int gCurrentLabel = 0;
std::map<int, Mat> gLabelImages;

std::vector<std::string> readLabels(const fs::path &labelPath) {
  std::vector<std::string> labels;
  std::ifstream labelFile(labelPath);

  while (std::getline(labelFile, labels.emplace_back()))
    ;

  labelFile.close();
  return labels;
}

void onMouse(int event, int x, int y, int flags, void *userdata) {
  Mat curFrame = frame.clone();

  if (event == cv::EVENT_LBUTTONDOWN) {
    gPosBeg.x = x;
    gPosBeg.y = y;
    gDrawing = true;
  }

  if (gDrawing) {
    const auto& [x0, x1] = std::minmax(gPosBeg.x, x);
    const auto& [y0, y1] = std::minmax(gPosBeg.y, y);
    const Rect rect(x0, y0, x1 - x0, y1 - y0);

    if (event == cv::EVENT_LBUTTONUP) {
      if (rect.area() > 10) {
        gRects.push_back({rect, gCurrentLabel});
      }

      gDrawing = false;
    }

    cv::rectangle(curFrame, gPosBeg, Point(x, y), Scalar(255, 0, 255), 2);
  }

  const auto &text = gLabelImages[gCurrentLabel];
  cv::addWeighted(text, 0.6, curFrame(Rect(0, 0, text.cols, text.rows)), 0.4,
                  1.0, curFrame(Rect(0, 0, text.cols, text.rows)));
  cv::line(curFrame, Point(x, 0), Point(x, frame.rows), Scalar(0, 255, 255), 2);
  cv::line(curFrame, Point(0, y), Point(frame.cols, y), Scalar(0, 255, 255), 2);
  for (const auto &[rect, label] : gRects) {
    cv::rectangle(curFrame, rect, Scalar(255, 0, 0), 2);
  }

  cv::imshow("frame", curFrame);
}

size_t getLastIndex(const fs::path &indexPath) {
  std::ifstream indexFile(indexPath);

  size_t val;
  indexFile >> val;
  indexFile.close();
  return val;
}

void updateLastIndex(const fs::path &indexPath, size_t index) {
  std::ofstream indexFile(indexPath, std::ios_base::out | std::ios_base::trunc);

  indexFile << index;
  indexFile.close();
}

int main(int argc, const char **argv) {
  ArgumentParser parser;
  parser.addArgument("-v", "--video", 1);
  parser.addArgument("-l", "--labels", 1);
  parser.addArgument("-o", "--output", 1);
  parser.parse(argc, argv);

  const auto nofile =
      std::make_error_code(std::errc::no_such_file_or_directory);

  const fs::path videoPath(parser.retrieve<std::string>("video"));
  if (not fs::exists(videoPath)) {
    throw fs::filesystem_error("File not found", videoPath, nofile);
  }

  const fs::path labelsPath(parser.retrieve<std::string>("labels"));
  if (not fs::exists(labelsPath)) {
    throw fs::filesystem_error("File not found", labelsPath, nofile);
  }

  const fs::path outputDirPath(parser.retrieve<std::string>("output"));
  if (not fs::exists(outputDirPath)) {
    throw fs::filesystem_error("Directory not found", outputDirPath, nofile);
  }

  const fs::path indexPath(outputDirPath / "index.txt");

  gIndex = getLastIndex(indexPath);
  gLabels = readLabels(labelsPath);

  for (size_t label = 0; label < gLabels.size(); label++) {
    gLabelImages[label] = Mat::zeros(50, 350, CV_8UC3);
    cv::putText(gLabelImages[label], gLabels[label], Point(5, 30),
                cv::FONT_HERSHEY_PLAIN, 2.0, Scalar(0, 0, 255), 3);
  }

  cv::VideoCapture cap(videoPath);
  cv::namedWindow("frame", 0);
  cv::setMouseCallback("frame", onMouse, 0);
  cv::createTrackbar("Objects", "frame", &gCurrentLabel, gLabels.size() - 1);

  bool newFrame = true;

  while (cap.isOpened()) {
    if (newFrame) {
      cap >> frame;
      cv::imshow("frame", frame);
    }

    newFrame = true;
    char c = cv::waitKey(1000);

    if (c == 27) // esc key
    {
      break;
    } else if (c == 's') {
      const auto jpgPath =
          (outputDirPath / std::to_string(gIndex)).replace_extension("jpg");
      const auto txtPath =
          (outputDirPath / std::to_string(gIndex)).replace_extension("txt");
      std::ofstream txtFile(txtPath.c_str(),
                            std::ios_base::out | std::ios_base::trunc);

      for (const auto &[rect, label] : gRects) {
        float xr = (rect.x + rect.width / 2.0) * 1.0 / frame.cols;
        float yr = (rect.y + rect.height / 2.0) * 1.0 / frame.rows;
        float wr = (rect.width) * 1.0 / frame.cols;
        float hr = (rect.height) * 1.0 / frame.rows;

        txtFile << label << " " << xr << " " << yr << " " << wr << " " << hr
                << std::endl;
      }

      txtFile.close();

      updateLastIndex(indexPath, ++gIndex);
      cv::imwrite(jpgPath, frame);
    } else if (c == 'x') {
      gRects.clear();
      newFrame = false;
    } else if (c == 'd') // 'd' key - move to next frame
    {
    } else if (c == 'w') // 'w' key - skip one second
    {
      for (size_t i = 0; i < cap.get(cv::CAP_PROP_FPS); i++)
        cap >> frame;
    } else {
      newFrame = false;
    }
  }

  return 0;
}
